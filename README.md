# Seasons 19

![For Farming Simulator 19](https://img.shields.io/badge/Farming%20Simulator-19-FF7C00.svg) [![Build Status](https://travis-ci.org/RealismusModding/FS19_RM_Seasons.svg?branch=develop)](https://travis-ci.org/RealismusModding/FS19_RM_Seasons)

***WARNING***: You are looking at the DEVELOPMENT VERSION of Seasons 19. To play Seasons, download at the Giants ModHub: [https://farming-simulator.com/mod.php?mod_id=79288](https://farming-simulator.com/mod.php?mod_id=79288). The code here might break your savegame, and computer, and give a lot of pain.

***WARNING***: We do not officially support activating Seasons on an existing savegame. Do this at your own risk. Read more about this in the guide.

Please do give feedback on [Slack](http://slack.realismusmodding.com) and post any issues you find!

## Publishing
Only the Realismus Modding team is allowed to publish any of this code as a mod to any mod site, or file sharing site.
The code is open for your own use, but give credit where due.
Thus, when building your own version of Seasons, give a credit notice to Realismus Modding when publishing screenshots or images.
This is not required when using the only official published mod by Realismis Modding, on Giants ModHub. It would be a nice gesture however.

## Making videos of development version
We have a couple of rules regarding videos to keep both our and our players experience the best. You are allowed to make videos with Seasons, under a couple of simple conditions:
- Do not share the GitHub link, if you are using a development version of the mod.
- Do not explain how to install this mod. (The mod might also change at any moment, making your video outdated)
- Give credit to 'Realismus Modding' as creators of the mod.
- Link to our [YouTube channel](https://www.youtube.com/c/realismusmodding).
- You do _not_ need to put the mod name in the video title, but you can if you want.

- Join us on [Slack](http://slack.realismusmodding.com) and tell us all about your awesome video.

Videos that are not holding to the rules will get a request for removal.
If you have any questions about this policy you can ask them on Slack.

## Forking the code
You are allowed to fork this repository, apply changes and use those for private use.
You are not allowed to distribute these changes yourself.
Please open a pull request to allow for merging back your adjustments. (See also 'Publishing')

## Pull Requests
Please join us on [Slack](http://slack.realismusmodding.com) to discuss any changes you wish to make via pull requests, otherwise they are likely to be rejected, except for translations.

## For map makers
For a better game experience, custom maps should add one new density layer: Seasons mask. See [here](https://www.realismusmodding.com/mods/seasons19/manual/modding/info-for-map-makers) for more information, or join our [Slack](http://slack.realismusmodding.com).

## Features

TODO: write / copy from Seasons17

## Mod conflicts
Do not load multiple map mods. Only load the map you are using!

- Any mod that manipulates the weather, e.g. Multi overlay hud
- Any mod that manipulates growth
- Any mod that changes animals
- Any 'season manager' type mods
- Some animal cleaning mods may not work correctly, especially during the winter

## Contributing
You are free to contribute any code. But by doing this you transfer your copyright to the Realismus
Modding team so that we can publish this mod and change the code. Only we are allowed to publish Seasons.

Please follow the following code formatting rules:
- End your lines with \r\n (CRLF), not LF
- Use spaces, not tabs

## Copyright
Copyright (c) 2016-2019 Realismus Modding
All rights reserved.

This copyright does not impugn any trademarks or copyrights owned by GIANTS Software GmbH.

*Warranty disclaimer*. You agree that you are using the software solely at your own risk.
Realismus Modding provides the software “as is” and without warranty of any kind, and Realismus
Modding for itself and its publishers and licensors hereby disclaims all express or implied
warranties, including without limitation warranties of merchantability, fitness for a particular
purpose, performance, accuracy, reliability, and non-infringement.

The [Terms and Conditions of GIANTS Software GmbH](https://www.farming-simulator.com/termsModHub.php) also apply.

### Informal explanation
An informal explanation of what this all means (this part is not legalese and can't be treated as such)

Realismus Modding (we) wrote the Mod and we have the copyright on this Mod. That means the code is ours and we can do
with it what we want. It also means you can't just copy our code and use it for your own projects.
Only we can distribute the Mod to others.
We allow you to make small changes for your own gameplay or with your friends. But you are not allowed to
publish these changes openly.
When you make a contribution (translations, code changes) you give that code to us. Otherwise we would
not be able to publish this Mod anymore.
If you lose your savegame or if your computer crashes due to our Mod, you can tell us and we will
attempt to fix the mod but we will not be paying for a new computer nor getting your savegame back.
(Make backups!)
