----------------------------------------------------------------------------------------------------
-- SeasonsWorkArea
----------------------------------------------------------------------------------------------------
-- Purpose:  Update work areas to only work depending on new ground conditions
--
-- Copyright (c) Realismus Modding, 2018
----------------------------------------------------------------------------------------------------

SeasonsWorkArea = {}

function SeasonsWorkArea.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(WorkArea, specializations)
end

function SeasonsWorkArea.registerEvents(vehicleType)
end

function SeasonsWorkArea.registerFunctions(vehicleType)
end

function SeasonsWorkArea.registerOverwrittenFunctions(vehicleType)
    SpecializationUtil.registerOverwrittenFunction(vehicleType, "getIsWorkAreaActive", SeasonsWorkArea.getIsWorkAreaActive)
    SpecializationUtil.registerOverwrittenFunction(vehicleType, "loadWorkAreaFromXML", SeasonsWorkArea.loadWorkAreaFromXML)
end

function SeasonsWorkArea.registerEventListeners(vehicleType)
    SpecializationUtil.registerEventListener(vehicleType, "onLoad", SeasonsWorkArea)
    SpecializationUtil.registerEventListener(vehicleType, "onDraw", SeasonsWorkArea)
end

function SeasonsWorkArea.initSpecialization()
end

function SeasonsWorkArea:onLoad(savegame)
    local spec = self:seasons_getSpecTable("seasonsWorkArea")

    spec.showGroundFrozenWarning = false
end

function SeasonsWorkArea:getIsWorkAreaActive(superFunc, workArea)
    local spec = self:seasons_getSpecTable("seasonsWorkArea")
    spec.showGroundFrozenWarning = false

    if not superFunc(self, workArea) then
        return false
    end

    if workArea.seasons_ignoresFrozenSoil then
        return true
    end

    if g_seasons.weather:isGroundFrozen() then
        local allowed = g_seasons.vehicle.data:getIsWorkAreaTypeAllowedWithFrozenSoil(workArea.type)
        if not allowed then
            spec.showGroundFrozenWarning = true
            return false
        end
    end

    return true
end

function SeasonsWorkArea:loadWorkAreaFromXML(superFunc, workArea, xmlFile, key)
    if not superFunc(self, workArea, xmlFile, key) then
        return false
    end

    workArea.seasons_ignoresFrozenSoil = Utils.getNoNil(getXMLBool(xmlFile, key .. "#ignoresFrozenSoil"), false)

    return true
end

function SeasonsWorkArea:onDraw(isActiveForInput, isActiveForInputIgnoreSelection, isSelected)
    local spec = self:seasons_getSpecTable("seasonsWorkArea")
    if spec.showGroundFrozenWarning then
        g_currentMission:showBlinkingWarning(g_i18n:getText("seasons_warning_soilIsFrozen"))
    end
end
